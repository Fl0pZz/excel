import store from 'store2'
import lexer from '../lexer'
import parser from '../parser'

import { Sin, Len, Abs } from '../types/functions'
import { LeftParenthesis, RightParenthesis, Quote } from '../types/token'
import { Add, Subtract, Divide, Multiply } from '../types/operators'
import { Cell } from '../types/links'

const assert = (a, b, str, ...args) => {
  console.assert(a.toString() === b.toString(), a, b, ...args);
};

export const testLexer = () => {
  let l = lexer('12');
  assert([...l], [12]);

  l = lexer('-  12  +  3');
  assert([...l], [new Subtract(), 12, new Add(), 3]);

  l = lexer('(1 + 2)');
  assert([...l], [new LeftParenthesis(), 1, new Add(), 2, new RightParenthesis()]);

  l = lexer('12+345-678*910');
  assert([...l], [12, new Add(), 345, new Subtract(), 678, new Multiply(), 910]);

  l = lexer('ABS(-12)');
  assert([...l], [new Abs(), new LeftParenthesis(), new Subtract(), 12, new RightParenthesis()]);

  l = lexer('SIN(-12)');
  assert([...l], [new Sin(), new LeftParenthesis(), new Subtract(), 12, new RightParenthesis()]);

  l = lexer('"abc"');
  assert([...l], ['"abc"']);
  l = lexer('LEN("abc")');
  assert([...l], [new Len(), new LeftParenthesis(), '"abc"', new RightParenthesis()]);

  l = lexer('SIN(LEN("abc"))');
  assert([...l], [new Sin(), new LeftParenthesis(), new Len(), new LeftParenthesis(),
    '"abc"', new RightParenthesis(), new RightParenthesis()]);

  l = lexer('- 12 + 3 + SIN(LEN("abc"))');
  assert([...l], [new Subtract(), 12, new Add(), 3, new Add(), new Sin(), new LeftParenthesis(), new Len(),
    new LeftParenthesis(), '"abc"', new RightParenthesis(), new RightParenthesis()]);

  store.set('A1', '-  12  +  3');
  l = lexer('A1');
  assert([...l], [new Cell('A1')]);

  l = lexer('A1 + A1');
  assert([...l], [new Cell('A1'), new Add(), new Cell('A1')]);
  store.remove('A1')
};

export const testParser = () => {
  assert(parser(lexer('12')), 12);
  assert(parser(lexer('-12')), -12);
  assert(parser(lexer('12 - 3')), 9);
  assert(parser(lexer('12 + 3')), 15);
  assert(parser(lexer('12 * 3')), 36);
  assert(parser(lexer('12 / 3')), 4);
  assert(parser(lexer('-12 + 3')), -9);

  assert(parser(lexer('(1 + 2)')), 3);
  assert(parser(lexer('-(12 + 3)')), -15);
  assert(parser(lexer('12 + 345 - 678 * 910')), -616623);

  assert(parser(lexer('ABS(-12)')), 12);
  assert(parser(lexer('SIN(-12)')), Math.sin(-12));
  assert(parser(lexer('"abc"')), '"abc"');
  assert(parser(lexer('LEN("abc")')), 3);
  assert(parser(lexer('SIN(LEN("abc"))')), Math.sin(3));
  assert(parser(lexer('-12+3+SIN(LEN("abc"))')), -12 + 3 + Math.sin(3));

  store.set('A1', { value: -9, expr: '=-  12  +  3' });
  assert(parser(lexer('A1')), -9);
  assert(parser(lexer('1 + A1')), -8);
  assert(parser(lexer('A1 + A1')), -18);
  assert(parser(lexer('-A1')), 9);
  store.remove('A1')
};
