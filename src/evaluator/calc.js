import parser from './parser'
import lexer from './lexer'

export default function calc(str) {
  // let l = lexer(str);
  // return parser([...l])
  return parser(lexer(str))
}
