import { LeftParenthesis, RightParenthesis } from './types/token'
import { Abs, Sin, Len } from './types/functions'
import { Subtract, Add, Multiply, Divide } from './types/operators'
import { Cell } from './types/links'
import lexer from './lexer'
import type from './types/type'

function parser(lexed) {
  const next = () => {
    let a = lexed.next().value;
    // console.log('next', a);
    return a;
  };
  const isFunc = (t) => [Abs, Sin, Len].some(f => t instanceof f);
  let token;
  // expr : term { (+|-) term }
  // term : fact { (*|/) fact }
  // fact : number | '(' expr ')'
  function fact() {
    token = next();
    // console.log(1, 'token', token);
    if (token instanceof Number) {
      let tmp = token;
      token = next();
      // console.log('fact exit token', token);
      return tmp
    } else if (token instanceof String) {
      let tmp = token;
      token = next();
      // console.log('fact exit token', token);
      return tmp
    } else if (token instanceof Cell) {
      // console.log(token._cell);
      let tmp = token;
      token = next();
      // return parser(lexer(token.exec()))
      return Number(tmp.exec())
        ? new Number(tmp.exec())
        : new String(tmp.exec())
    }
    // console.log('exit fact');
    if (token instanceof LeftParenthesis) {
      let val = expr();
      if (!(token instanceof  RightParenthesis)) { throw new SyntaxError('Unpaired parentheses') }
      return val
    } else if (isFunc(token)) {
      // console.log('bbbbb');
      return token.exec(fact());
    } else if (token instanceof Subtract) {
      // console.log('aaa', token);
      return Multiply.exec(new Number(-1), fact());
    } else if (token instanceof String) {
      return token
    }
    console.log('anc', token, token instanceof String);
    throw new TypeError('Unknown type')
  }

  function term() {
    let val = fact();
    // console.log(2, 'val', val);
    while (token instanceof Multiply || token instanceof Divide) {
      val = token.exec(val, term())
    }
    return val;
  }

  function expr() {
    let val = term();
    // console.log(3, 'val', val);
    while (token instanceof Add || token instanceof Subtract) {
      val = token.exec(val, term())
    }
    return val;
  }
  return expr();
}

export default parser;
