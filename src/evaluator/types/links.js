import AbstractToken from './token'
// import store from 'store2'
import store from '../../store/store'

export class Cell extends AbstractToken {
  static is(str) { return /[A-Z][0-9]+/.test(str) }
  static get name() { return 'CELL' }

  constructor (str) {
    super();
    this._cell = str
  }

  exec() {
    if (store.state[this._cell].value !== '') {
      return store.state[this._cell].value;
    }
    throw new ReferenceError(`${this._cell} is empty!`)
  }
}
