import Cell from './cell/Cell.vue'
import Test from './evaluator/tests/Tests.vue'

export default {
  name: 'app',
  components: { Cell, Test },
  data: () => ({
    alphabet: []
  }),

  created () {
    this.alphabet = [...new Array(26)].map((val, i) => String.fromCharCode(i + 65));
  }
}
