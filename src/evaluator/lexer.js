import type, { isOperator, isFunc } from './types/type'
import { Space } from './types/ignored'
import { Quote, LeftParenthesis, RightParenthesis } from './types/token'
import { Cell } from './types/links'

function* tokenize(str) {
  let tmp = '';
  let countQuote = 0;

  for (let symbol of str) {
    // console.log(0, `syn |${symbol}|`, tmp);
    if (Space.is(symbol)) { continue }
    // console.log(1, 'symbol', symbol, tmp);
    if (!Quote.is(symbol) && (isOperator(symbol) || LeftParenthesis.is(symbol) || RightParenthesis.is(symbol))) {
      // console.log(2, 'symbol', symbol, tmp);
      if (tmp !== '') {
        // console.log('tmp', tmp);
        if (isFunc(tmp) || Number(tmp) || Cell.is(tmp)) {
          yield type(tmp);
          tmp = ''
        }
      }
      // console.log('symbol', symbol, type(symbol));
      yield type(symbol);
      continue
    } else if (Quote.is(symbol)) {
      ++countQuote;
      if (countQuote % 2 === 0) {
        yield new String(tmp + symbol);
        tmp = '';
        symbol = ''; //?
        countQuote = 0
      }
    }
    tmp += symbol
  }

  if (tmp && (isFunc(tmp) || Number(tmp) || Cell.is(tmp))) {
    yield type(tmp)
  }
}

export default tokenize
