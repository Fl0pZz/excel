import AbstractToken from './token'

export class Space extends AbstractToken {
  static is(str) { return /\s/.test(str) }
  static get name() { return 'SPACE' }

  constructor () { super() }
}
