import { Sin, Len, Abs } from './functions'
import { Space } from './ignored'
import { LeftParenthesis, RightParenthesis, Quote } from './token'
import { Add, Subtract, Divide, Multiply } from './operators'
import { Cell } from './links'

const type = (str) => {
  if (Sin.is(str)) { return new Sin() }
  else if (Len.is(str)) { return new Len() }
  else if (Abs.is(str)) { return new Abs() }
  else if (Space.is(str)) { return new Space() }
  else if (LeftParenthesis.is(str)) { return new LeftParenthesis() }
  else if (RightParenthesis.is(str)) { return new RightParenthesis() }
  else if (Add.is(str)) { return new Add() }
  else if (Subtract.is(str)) { return new Subtract() }
  else if (Divide.is(str)) { return new Divide() }
  else if (Quote.is(str)) { return new Quote() }
  else if (Multiply.is(str)) { return new Multiply() }
  else if (Cell.is(str)) { return new Cell(str) }
  else if (Number(str)) { return new Number(str) }
  else { return new String(str) }
};

export const isFunc = (str) => [Abs, Sin, Len].some(op => op.is(str));
export const isOperator = (str) =>  [Add, Subtract, Divide, Multiply].some(op => op.is(str));

export default type
