import AbstractToken from './token'

export default class AbstractFunc extends AbstractToken {
  static is(str) {
    if (this === AbstractFunc) {
      // Error Type 2. Abstract methods can not be called directly.
      throw new TypeError('Can not call static abstract method \'is(str)\'');
    } else if (this.is === AbstractFunc.is) {
      // Error Type 3. The child has not implemented this method.
      throw new TypeError('Please implement static abstract method \'is(str)\'');
    } else {
      // Error Type 5. The child has implemented this method but also called `super.foo()`.
      throw new TypeError('Do not call static abstract method \'is(str)\' from child');
    }
  }
  static get name() {
    if (this === AbstractFunc) {
      // Error Type 2. Abstract methods can not be called directly.
      throw new TypeError('Can not call static abstract getter \'name\'');
    } else if (this.name === AbstractFunc.name) {
      // Error Type 3. The child has not implemented this method.
      throw new TypeError('Please implement static abstract getter \'name\'');
    } else {
      // Error Type 5. The child has implemented this method but also called `super.foo()`.
      throw new TypeError('Do not call static abstract getter \'name\' from child');
    }
  }
  static exec () {
    if (this === AbstractFunc) {
      // Error Type 2. Abstract methods can not be called directly.
      throw new TypeError('Can not call static abstract method \'exec()\'');
    } else if (this.exec === AbstractFunc.exec) {
      // Error Type 3. The child has not implemented this method.
      throw new TypeError('Please implement static abstract method \'exec()\'');
    } else {
      // Error Type 5. The child has implemented this method but also called `super.foo()`.
      throw new TypeError('Do not call static abstract method \'exec()\' from child');
    }
  }

  constructor () {
    super();
    if (this.constructor === AbstractFunc) {
      // Error Type 1. Abstract class can not be constructed.
      throw new TypeError('Can not construct abstract class');
    }
    //else (called from child)
    // Check if all instance methods are implemented.
    if (this.exec === AbstractFunc.prototype.exec) {
      // Error Type 4. Child has not implemented this abstract method.
      throw new TypeError('Please implement abstract method \'exec()\'');
    }
  }

  exec () { throw new TypeError('Do not call abstract method \'exec()\' from child') }
}

export class Abs extends AbstractFunc {
  static is(str) { return /ABS/.test(str) }
  static get name() { return 'ABS' }
  static exec($1) {
    const argTypes = [Number];
    if (!argTypes.some(type => $1 instanceof type)) {
      throw new TypeError('Unexpected argument type')
    }
    return new Number(Math.abs($1))
  }
  constructor () { super() }
  exec($1) { return Abs.exec($1) }
}

export class Sin extends AbstractFunc {
  static is(str) { return /SIN/.test(str) }
  static get name() { return 'SIN' }
  static exec($1) {
    const argTypes = [Number];
    if (!argTypes.some(type => $1 instanceof type)) {
      throw new TypeError('Unexpected argument type')
    }
    return new Number(Math.sin($1))
  }
  constructor () { super() }
  exec($1) { return Sin.exec($1) }
}

export class Len extends AbstractFunc {
  static is(str) { return /LEN/.test(str) }
  static get name() { return 'LEN' }
  static exec($1) {
    const argTypes = [String];
    if (!argTypes.some(type => $1 instanceof type)) {
      throw new TypeError('Unexpected argument type')
    }
    return new Number($1.length - 2)
  }
  constructor () { super() }
  exec($1) { return Len.exec($1) }
}
