import AbstractFunc from './functions'

export class Add extends AbstractFunc {
  static is(str) { return /\+/.test(str) }
  static get name() { return 'ADD' }
  static exec($1, $2) {
    const argTypes = [Number, String];
    if (!argTypes.some(type => $1 instanceof type && $2 instanceof type)) {
      throw new TypeError('Unexpected argument type')
    }
    if ($1 instanceof Number) {
      return new Number($1 + $2)
    } else if ($1 instanceof String) {
      return new String($1 + $2)
    }
  }
  constructor () { super() }
  exec ($1, $2) { return Add.exec($1, $2) }
}

export class Subtract extends AbstractFunc {
  static is(str) { return /-/.test(str) }
  static get name() { return 'SUBTRACT' }
  static exec($1, $2) {
    const argTypes = [Number];
    if (!argTypes.some(type => $1 instanceof type && $2 instanceof type)) {
      throw new TypeError('Unexpected argument type')
    }
    return new Number($1 - $2)
  }
  constructor () { super() }
  exec($1, $2) { return Subtract.exec($1, $2) }
}

export class Multiply extends AbstractFunc {
  static is(str) { return /\*/.test(str) }
  static get name() { return 'MULTIPLY' }
  static exec($1, $2) {
    const argTypes = [Number];
    if (!argTypes.some(type => $1 instanceof type && $2 instanceof type)) {
      throw new TypeError('Unexpected argument type')
    }
    return new Number($1 * $2)
  }
  constructor () { super() }
  exec($1, $2) { return Multiply.exec($1, $2) }
}

export class Divide extends AbstractFunc {
  static is(str) { return /\//.test(str) }
  static get name() { return 'DIVIDE' }
  static exec($1, $2) {
    const argTypes = [Number];
    if (!argTypes.some(type => $1 instanceof type && $2 instanceof type)) {
      throw new TypeError('Unexpected argument type')
    }
    return new Number($1 / $2)
  }
  constructor () { super() }
  exec($1, $2) { return Divide.exec($1, $2) }
}
