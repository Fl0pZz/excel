import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const initState = () => {
  console.log('aaaa');
  const alphabet = [...new Array(26)].map((val, i) => String.fromCharCode(i + 65));
  let out = {};
  for (let i = 1; i < 21; ++i) {
    for (let s of alphabet) {
      out[s + i] = {
        needUpdate: false,
        expr: '',
        value: '',
        dependencies: [],
        dependOn: []
      }
    }
  }
  console.log('out', out);
  return out
};

const state = initState();

const store = new Vuex.Store({
  state,
  mutations: {
    ['UPDATE']: (state, { id, data }) => { state[id] = { needUpdate: false, ...data }},
    ['ADD_DEPENDENCY']: (state, { id, cell }) => {
      state[id].dependencies.push(cell)
    },
    ['REMOVE_DEPENDENCY']: (state, { id, cell }) => {
      console.log('remove', id, cell, state[id].dependencies, state[id].dependencies.findIndex(c => c === cell));
      let index = state[id].dependencies.findIndex(c => c === cell);
      if (index != -1) {
        state[id].dependencies.splice(index, 1)
      }
    },
    ['NEED_UPDATE']: (state, id) => {
      state[id].needUpdate = true
    },
    ['UPDATED']: (state, id) => {
      state[id].needUpdate = false
    }
  },
  actions: {
    update ({ commit, state }, { id, data }) {
      for (let cell of state[id].dependOn) {
        commit('REMOVE_DEPENDENCY', { id: cell, cell: id })
      }

      const { dependOn, dependencies } = data;
      for (let cell of dependOn) {
        commit('ADD_DEPENDENCY', { id: cell, cell: id })
      }
      commit('UPDATE', { id, data });
      for (let cell of dependencies) {
        commit('NEED_UPDATE', cell)
      }
    }
  }
});

export default store;
