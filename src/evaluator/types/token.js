export default class AbstractToken {
  static is(str) {
    if (this === AbstractToken) {
      // Error Type 2. Abstract methods can not be called directly.
      throw new TypeError('Can not call static abstract method \'is(str)\'');
    } else if (this.is === AbstractToken.is) {
      // Error Type 3. The child has not implemented this method.
      throw new TypeError('Please implement static abstract method \'is(str)\'');
    }
    else {
      // Error Type 5. The child has implemented this method but also called `super.foo()`.
      throw new TypeError('Do not call static abstract method \'is(str)\' from child');
    }
  }
  static get name() {
    if (this === AbstractToken) {
      // Error Type 2. Abstract methods can not be called directly.
      console.log('functions', this);
      throw new TypeError('Can not call static abstract getter \'name\'');
    } else if (this.name === AbstractToken.name) {
      // Error Type 3. The child has not implemented this method.
      throw new TypeError('Please implement static abstract getter \'name\'');
    } else {
      // Error Type 5. The child has implemented this method but also called `super.foo()`.
      throw new TypeError('Do not call static abstract getter \'name\' from child');
    }
  }

  constructor () {}
}

export class LeftParenthesis extends AbstractToken {
  static get name() { return 'LEFT_PARENTHESIS' }
  static is(str) { return /\(/.test(str) }

  constructor () { super() }
}

export class RightParenthesis extends AbstractToken {
  static get name() { return 'RIGHT_PARENTHESIS' }
  static is(str) { return /\)/.test(str) }

  constructor () { super() }
}

export class Quote extends AbstractToken{
  static get name() { return 'QUOTE' }
  static is(str) { return /"/.test(str) }

  constructor () { super() }
}

