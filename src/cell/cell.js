import calc from '../evaluator/calc'
import lexer from '../evaluator/lexer'
import { Cell } from '../evaluator/types/links'
import { mapState, mapActions, mapMutations } from 'vuex'

export default {
  name: 'cell',
  props: ['id'],
  data: () => ({
    placeholder: '',
    dataCell: {
      expr: '',
      value: '',
      dependencies: [],
      dependOn: []
    }
  }),

  watch: {
    needUpdate (newVal) {
      if (newVal) {
        this.recount();
        this.updated(this.id);
        this.placeholder = this.dataCell.value;
      }
    }
  },

  created () {
    this.dataCell.expr = this.$store.state[this.id].expr;
    this.dataCell.value = this.$store.state[this.id].value;
    this.dataCell.dependencies = this.$store.state[this.id].dependencies;
    this.dataCell.dependOn = this.$store.state[this.id].dependOn;
  },

  computed: {
    ...mapState({
      needUpdate (state) {
        return state[this.id].needUpdate
      }
    })
  },

  methods: {
    ...mapActions(['update']),
    ...mapMutations({ updated: 'UPDATED' }),
    recount () {
      try {
        if (this.dataCell.expr.length) {
          let l = lexer(this.dataCell.expr.slice(1, this.dataCell.expr.length));
          this.dataCell.dependOn = [...l].filter(token => token instanceof Cell)
            .map(c => (c._cell[0] === '=')
              ? c._cell.slice(1, c._cell.lenght)
              : c._cell);
          console.log('lexer', this.dataCell, [...l].filter(token => token instanceof Cell));
          this.dataCell.value = calc(this.dataCell.expr.slice(1, this.dataCell.expr.length));
        }
      } catch (e) {
        this.dataCell.value = e.message;
      }
      this.update({ id: this.id, data: this.dataCell })
    },
    onFocus () {
      this.placeholder = this.dataCell.expr;
      console.log('onFocus');
    },
    onBlur () {
      console.log('onBlur');
      this.dataCell.expr = this.placeholder;
      this.recount();
      this.placeholder = this.dataCell.value;
    }
  }
}
